<?php require_once "./code.php"; ?> 

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S03: Activity</title>
</head>
<body>
    <h1>Activity Session 3</h1>

    <h2>Person</h2>
    <p>
        <?php
            echo $person->printName();
        ?>
    </p>

    <h2>Developer</h2>
    <p>
        <?php
            echo $developer->printName();
        ?>
    </p>

    <h2>Engineer</h2>
    <p>
        <?php
            echo $engineer->printName();
        ?>
    </p>
    
</body>
</html>